# Kubernetes 101 

It is a way to manage and deploy containers at scale! Works like terraform but for containers, with some extra features. 

Includes:

- Self healing
- Monioroing
- Loved by the community

### How does it work?

K8 is setup as cluster (like ansible).

- k8 Control panel
- k8 worker nodes

Inside the nodes you have:

- Namespaces (like a vpc)
- Services (endpoinds for pods, like LB)
- Pods (container + launch templates)

### Pre requisits And pre install notes

There are different ways to get a k8 cluster. It's generally a bit tricky to set up by yourself. The following are options

https://kubernetes.io/docs/setup/production-environment/tools/

- Use kubespray (ansible)
- kubeadm

Other serives of kubernetes that are managed:

- aws EKS
- Google GKE

Different distro of K8:

- k8 offical
- minikube
- MicroK8s


### our setup 

We'll use kubespray:

- https://github.com/kubernetes-sigs/kubespray

We're going to need:

- 1 k8 control panel - t3a.xlarge
- 2 k8 woker nodes  - t3a.xlarge
- 1 ansible machine with the right access t3a.xlarge


1. Creat all your instances
2. ssh into the ansible machine
3. git clone the kubespray
4. intall the requierments
5. copy the inventory and add the need private ips of the Control panel + nodes
6. Allow networking between the machines
7. generate a new key pair for the ansible to conect to the Control panel + nodes
8. Add they public key to autorized_keys of Control panel + nodes